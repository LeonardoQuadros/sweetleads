Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  ########################################################################################################
  
  namespace :dashboard do
    
    controller :pages do
      get '/home' => :home
      get '/email' => :email
      get '/perfil' => :perfil
      get '/settings' => :settings
      post '/save_settings' => :save_settings
    end
    
    controller :users do
    end
    resources :users
    
    controller :authors do
    end
    resources :authors
    
    controller :books do
      get '/books/get_authors' => :get_authors, as: :books_get_authors
      get '/books/get_book' => :get_book, as: :books_get_book
    end
    resources :books
    
    controller :completeds do
    end
    resources :completeds
    
    mount ActionCable.server => '/dashboard/cable'
    
    controller :cycles do
      get '/cycles/channels/:id' => :channels, as: :cycles_channels
      get '/cycles/members/:id' => :members, as: :cycles_members
    end
    resources :cycles
    
    controller :channels do
      get '/channels/scroll/:id' => :scroll, as: :channels_scroll
    end
    resources :channels, only: [:show, :create, :destroy]
    
    controller :talks do
      get '/talks/scroll/:id' => :scroll, as: :talks_scroll
    end
    resources :talks, only: [:show]
    
    controller :tests do
      post '/tests/complete/:id' => :complete, as: :tests_complete
    end
    resources :tests 
    
    get '/:slug', to: 'cycles#show'
    resources :participations, only: [:create, :destroy]

  	root 'pages#home'
  	get '*path' => redirect('/')
  end

  ########################################################################################################


  devise_for :users, path: '', path_names: { sign_in: 'home', sign_out: 'logout', sign_up: "register"}

  authenticated :user do
  	root 'dashboard/pages#home', as: :authenticated_root
  end

  controller :pages do
  	get '/home' => :home
  	post '/login' => :login
  end

  root "pages#home"  
  get '*path' => redirect('/')

end
