FactoryBot.define do
  factory :cycle do
    slug {Faker::Lorem.word}
    description {Faker::Lorem.paragraph(10)}
    user
		book
		start_date {Date.today}
		end_date {Date.tomorrow}
  end
end
