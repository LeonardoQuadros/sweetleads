FactoryBot.define do
  factory :channel do
    slug { Faker::Lorem.word }
    cycle 
    user { cycle.user }
  end
end
