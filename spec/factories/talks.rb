FactoryBot.define do
  factory :talk do
    user_one_id { create(:user) }
    user_two_id { create(:user) }
    cycle
  end
end
