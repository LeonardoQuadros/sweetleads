FactoryBot.define do
  factory :book do
    name {Faker::Lorem.word}
  end
end
