require 'rails_helper'

RSpec.describe CyclesController, type: :controller do
  include Devise::Test::ControllerHelpers

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    @current_user = FactoryGirl.create(:user)
    sign_in @current_user
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do

    context "cycle exists" do
      context "User is the owner of the cycle" do
        it "Returns success" do
          cycle = create(:cycle, user: @current_user)
          get :show, params: {slug: cycle.slug}

          expect(response).to have_http_status(:success)
        end
      end

      context "User is member of the cycle" do
        it "Returns success" do
          cycle = create(:cycle)
          cycle.users << @current_user
          get :show, params: {slug: cycle.slug}

          expect(response).to have_http_status(:success)
        end
      end

      context "User is not the owner or member of the cycle" do
        it "Redirects to root" do
          cycle = create(:cycle)
          get :show, params: {slug: cycle.slug}

          expect(response).to redirect_to('/')
        end
      end
    end

    context "cycle don't exists" do
      it "Redirects to root" do
        cycle_attributes = attributes_for(:cycle)
        get :show, params: { slug: cycle_attributes[:slug] }

        expect(response).to redirect_to('/')
      end
    end

  end

  describe "POST #create" do
    before(:each) do
      @cycle_attributes = attributes_for(:cycle, user: @current_user)
      post :create, params: {cycle: @cycle_attributes}
    end

    it "Redirect to new cycle" do
      expect(response).to have_http_status(302)
      expect(response).to redirect_to("/#{@cycle_attributes[:slug]}")
    end

    it "Create cycle with right attributes" do
      expect(Cycle.last.user).to eql(@current_user)
      expect(Cycle.last.slug).to eql(@cycle_attributes[:slug])
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      request.env["HTTP_ACCEPT"] = 'application/json'
    end

    context "User is the Cycle Owner" do
      it "returns http success" do
        cycle = create(:cycle, user: @current_user)
        delete :destroy, params: {id: cycle.id}
        expect(response).to have_http_status(:success)
      end
    end

    context "User isn't the cycle Owner" do
      it "returns http forbidden" do
        cycle = create(:cycle)
        delete :destroy, params: {id: cycle.id}
        expect(response).to have_http_status(:forbidden)
      end
    end

    context "User is member of the cycle" do
      it "returns http forbidden" do
        cycle = create(:cycle)
        cycle.users << @current_user
        delete :destroy, params: {id: cycle.id}
        expect(response).to have_http_status(:forbidden)
      end
    end
  end
end