class CreateParticipations < ActiveRecord::Migration[5.2]
  def change
    create_table :participations do |t|
    	t.references 		:user, index: true
    	t.references 		:cycle, index: true
    	t.references 		:knowledge, index: true
    	t.references 		:autocritic, index: true
    	t.references 		:book_review, index: true
    	t.references 		:extra_content, index: true
    	t.boolean 			:agreed, default: false
    	t.boolean 			:concluded_25, default: false
    	t.boolean 			:concluded_50, default: false
    	t.boolean 			:concluded_75, default: false
    	t.boolean 			:concluded_100, default: false
      t.timestamps
    end
  end
end
