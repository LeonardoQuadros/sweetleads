class CreateTests < ActiveRecord::Migration[5.2]
  def change
    create_table :tests do |t|
    	t.string 					:name
    	t.text 					:description
    	t.date 					:date
    	t.references 					:cycle
    	t.references 					:test
      t.integer           :order
    	t.boolean 					:dissertative, default: false
      t.timestamps
    end
  end
end
