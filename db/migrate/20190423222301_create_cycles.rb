class CreateCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :cycles do |t|
      t.text  :description
      t.text  :notes
      t.text  :links
    	t.references :user, index: true
    	t.references :book, index: true
    	t.date 			:start_date
    	t.date 			:end_date
      t.timestamps
    end
  end
end
