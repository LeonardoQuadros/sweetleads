class CreateCompleteds < ActiveRecord::Migration[5.2]
  def change
    create_table :completeds do |t|
    	t.references 						:user, index: true
    	t.references 						:test, index: true
    	t.boolean 							:completed, default: false
    	t.text 									:content
    	t.date 									:completed_date
      t.timestamps
    end
  end
end
