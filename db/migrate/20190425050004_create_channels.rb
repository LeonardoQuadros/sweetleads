class CreateChannels < ActiveRecord::Migration[5.2]
  def change
    create_table :channels do |t|
      t.string :slug
      t.references :user, foreign_key: true
      t.references :cycle, foreign_key: true

      t.timestamps
    end
  end
end
