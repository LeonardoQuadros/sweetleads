class AddSlugToCycle < ActiveRecord::Migration[5.2]
  def change
  	add_column :cycles, :slug, :string
  end
end
