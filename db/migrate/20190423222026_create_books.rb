class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
    	t.string :name
    	t.text :description
    	t.string :link
      t.string :scraping
    	t.string :image
    	t.json :features, default: {}
    	t.references :author, index: true
      t.timestamps
    end
  end
end
