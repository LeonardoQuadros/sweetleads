class AddCamposComplementaresToUsers < ActiveRecord::Migration[5.2]
  def change
  	add_column            :users, :token, :string
  	add_column            :users, :nick, :string
  	add_column            :users, :name, :string
  	add_column            :users, :phone, :string
  	add_column            :users, :admin, :boolean, default: false
  end
end
