# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_27_052731) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "authors", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "biography"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "books", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "link"
    t.string "scraping"
    t.string "image"
    t.json "features", default: {}
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_books_on_author_id"
  end

  create_table "channels", force: :cascade do |t|
    t.string "slug"
    t.bigint "user_id"
    t.bigint "cycle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cycle_id"], name: "index_channels_on_cycle_id"
    t.index ["user_id"], name: "index_channels_on_user_id"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "data_fingerprint"
    t.string "type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "completeds", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "test_id"
    t.boolean "completed", default: false
    t.text "content"
    t.date "completed_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["test_id"], name: "index_completeds_on_test_id"
    t.index ["user_id"], name: "index_completeds_on_user_id"
  end

  create_table "cycles", force: :cascade do |t|
    t.text "description"
    t.text "notes"
    t.text "links"
    t.bigint "user_id"
    t.bigint "book_id"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["book_id"], name: "index_cycles_on_book_id"
    t.index ["user_id"], name: "index_cycles_on_user_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.integer "user_id"
    t.integer "messagable_id"
    t.string "messagable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "participations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "cycle_id"
    t.bigint "knowledge_id"
    t.bigint "autocritic_id"
    t.bigint "book_review_id"
    t.bigint "extra_content_id"
    t.boolean "agreed", default: false
    t.boolean "concluded_25", default: false
    t.boolean "concluded_50", default: false
    t.boolean "concluded_75", default: false
    t.boolean "concluded_100", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["autocritic_id"], name: "index_participations_on_autocritic_id"
    t.index ["book_review_id"], name: "index_participations_on_book_review_id"
    t.index ["cycle_id"], name: "index_participations_on_cycle_id"
    t.index ["extra_content_id"], name: "index_participations_on_extra_content_id"
    t.index ["knowledge_id"], name: "index_participations_on_knowledge_id"
    t.index ["user_id"], name: "index_participations_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "talks", force: :cascade do |t|
    t.integer "user_one_id"
    t.integer "user_two_id"
    t.bigint "cycle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cycle_id"], name: "index_talks_on_cycle_id"
  end

  create_table "tests", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.date "date"
    t.bigint "cycle_id"
    t.bigint "test_id"
    t.integer "order"
    t.boolean "dissertative", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cycle_id"], name: "index_tests_on_cycle_id"
    t.index ["test_id"], name: "index_tests_on_test_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "token"
    t.string "nick"
    t.string "name"
    t.string "phone"
    t.boolean "admin", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "channels", "cycles"
  add_foreign_key "channels", "users"
  add_foreign_key "talks", "cycles"
end
