class PagesController < PublicController

	def home
	end

	def login
		sign_in(resource_name, resource)
	end

end
