class Dashboard::UsersController < Dashboard::ApplicationController

  before_action :set_user, only: [:show, :edit, :update, :destroy]

	def index
		conditions = []
		conditions << "users.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
		@users = User.where(conditions.join(" AND ")).order("created_at desc")
	end

	def show
	end

	def edit
	end

	def update
    params[:user].delete(:password) if params[:user][:password].blank?
		if @user.update(user_params)
			if params[:user][:fotos].blank?
				respond_to do |format|
					format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
				end
			else
				redirect_to request.referrer, notice: "Updated"
			end
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: '#{current_user.errors.messages}', classes: 'red'}); " }
			end
		end
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			redirect_to edit_dashboard_user_path(id: @user.id), notice: "User #{@user.name} created successfully!"
		else
			respond_to do |format|
				format.js
			end
		end
	end

	def destroy
		if @user.destroy
			redirect_to request.referrer, notice: "Usuário deletado!"
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Unable to delete user #{@user.name}', classes: 'red'}); " }
			end
		end
	end

	private

	def user_params
		params.require(:user).permit(:name, :email, :password, :phone)
	end
	def set_user
		@user = User.find(params[:id])
	end

end
