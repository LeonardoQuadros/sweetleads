class Dashboard::ChannelsController < Dashboard::ApplicationController
  before_action :set_channel, only: [:destroy, :show, :scroll]

  def create
    @channel = Channel.new(channel_params)
    authorize! :create, @channel

    respond_to do |format|
      if @channel.save
        format.json { render :show, status: :created }
      else
        Rails.logger.debug("  ============== Exception:  #{@channel.errors.full_messages} ")

        format.json { render json: @channel.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    authorize! :destroy, @channel
    @channel.destroy

    respond_to do |format|
      format.json { render json: true }
    end
  end

  def show
    session[:per_page] = 1
    authorize! :read, @channel
  end

  def scroll
    session[:per_page] += 1 unless session[:per_page] > @channel.messages.page.total_pages 
    authorize! :read, @channel
  end

  private

  def set_channel
    @channel = Channel.find(params[:id])
  end

  def channel_params
    params.require(:channel).permit(:slug, :cycle_id).merge(user: current_user)
  end
end