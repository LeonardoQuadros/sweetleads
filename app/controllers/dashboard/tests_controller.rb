class Dashboard::TestsController < Dashboard::ApplicationController
  include TestHelper
  include CompletedHelper

  before_action :set_test, only: [:show, :edit, :update, :destroy, :complete]

  def index
    authorize! :config, :backend
    conditions = []
    conditions << "tests.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
    conditions << "UPPER(unaccent(tests.name)) LIKE '%#{I18n.transliterate(params[:name].upcase)}%'" unless params[:name].blank?
    @tests = Test.where(conditions.join(" AND ")).order("created_at desc")
  end

  def show
    @test.join(current_user)
    @completed = @test.completeds.where(user_id: current_user.id).first if @test.dissertative
  end

  def edit
  end

  def complete
    if test_actual(@test)
      t = @test
      if @test = @test.complete(current_user)
        if @test.errors.any?
          respond_to do |format|
            format.js
          end
        else
          SendMailer.email_activity_completion(current_user, t).deliver_later
          redirect_to request.referrer, notice: "Updated!"
        end
      else
        respond_to do |format|
          format.js
        end
      end
    else
      respond_to do |format|
        message = "Complete the required test before - #{@test.test.name unless @test.test.blank?}"
        format.js {render inline: "M.toast({html: '#{message}', classes: 'orange'}); " }
      end
    end
  end

  def update
    authorize! :config, :backend
    if @test.update(test_params)
      @test.cycle.ordenar!
        respond_to do |format|
          format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
        end
    else
      respond_to do |format|
        format.js {render inline: "M.toast({html: '#{@test.errors.messages}', classes: 'red'}); " }
      end
    end
  end

  def new
    @test = Test.new
    @test.cycle = Cycle.find(params[:cycle_id])
  end

  def create
    authorize! :config, :backend
    @test = Test.new(test_params)
    if @test.save
      @test.cycle.ordenar!
      redirect_to edit_dashboard_test_path(id: @test.id), notice: "Test #{@test.name} created successfully!"
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def destroy
    authorize! :config, :backend
    name = @test.name
    @cycle = @test.cycle
    if @test.destroy
      @cycle.ordenar!
      redirect_to request.referrer, notice: "#{name} has been deleted!"
    else
      respond_to do |format|
        format.js {render inline: "M.toast({html: 'Unable to delete test #{@test.name}', classes: 'red'}); " }
      end
    end
  end

  private

  def test_params
    params.require(:test).permit(:name, :description, :cycle_id, :date, :test_id, :dissertative)
  end
  def set_test
    @test = Test.find(params[:id])
  end

end
