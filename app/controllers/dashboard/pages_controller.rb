class Dashboard::PagesController < Dashboard::ApplicationController
	
	def home
	end

	def email
		@user = current_user
		@cycle = Cycle.first
	end

	def settings
    authorize! :config, :backend
	end

	def save_settings
    authorize! :config, :backend
		Setting.email_confirmation_commitment_subject										= params[:email_confirmation_commitment_subject]
		Setting.email_confirmation_commitment														= params[:email_confirmation_commitment]
		Setting.email_activity_completion_subject												= params[:email_activity_completion_subject]
		Setting.email_activity_completion																= params[:email_activity_completion]
		Setting.email_activity_deadline_coming_end_subject							= params[:email_activity_deadline_coming_end_subject]
		Setting.email_activity_deadline_coming_end											= params[:email_activity_deadline_coming_end]
		
		respond_to do |format|
			format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
		end
	end

end
