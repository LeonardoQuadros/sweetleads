class Dashboard::ParticipationsController < Dashboard::ApplicationController
  before_action :set_participation, only: [:destroy]

  def create
    @participation = Participation.new(participation_params)

      if @participation.save
        SendMailer.email_confirmation_commitment(current_user, @participation.cycle).deliver_later
        redirect_to dashboard_cycle_path(@participation.cycle), notice: "Joined to #{@participation.cycle.slug}!"
      else
        respond_to do |format|
        end
      end
  end

  def destroy
    authorize! :destroy, @participation
    @participation.destroy

    respond_to do |format|
      format.json { render json: true }
    end
  end

  private

  def set_participation
    @participation = Participation.find_by(user_id: params[:id], cycle_id: params[:cycle_id])
  end

  def participation_params
    params.require(:participation).permit(:cycle_id).merge(user_id: current_user.id)
  end
end