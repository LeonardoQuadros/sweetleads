class Dashboard::BooksController < Dashboard::ApplicationController

	require './app/services/book_service'
  before_action :set_book, only: [:show, :edit, :update, :destroy]

	def index
    authorize! :config, :backend
		conditions = []
		conditions << "books.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
		@books = Book.where(conditions.join(" AND ")).order("created_at desc")
	end

	def get_authors
		conditions = []
		conditions << "UPPER(unaccent(authors.name)) LIKE '%#{I18n.transliterate(params[:name].upcase)}%'"

		@authors = Author.where(conditions.join(' AND ')).order("name asc")
	end

	def get_book
		@list = {}
		@list = BookService.get_book(params[:title])
		@features = @list.blank? ? {} : @list[:features]
		session[:features] = @features

	end

	def show
		@cycle = @book.cycles.first
	end

	def edit
		@features = @book.features
	end

	def update
    authorize! :config, :backend
		if @book.update(book_params)
			if params[:book][:fotos].blank?
				respond_to do |format|
					format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
				end
			else
				redirect_to request.referrer, notice: "Updated"
			end
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: '#{@book.errors.messages}', classes: 'red'}); " }
			end
		end
	end

	def new
		@book = Book.new
		session.delete(:features)
	end

	def create
    authorize! :config, :backend
		@book = Book.new(book_params)

		unless session[:features].blank?
			@book.features = session[:features] 
			session.delete(:features)
		end

		Book.transaction do
			author_name = params[:author].gsub(',', ' ').gsub(';', ' & ').gsub('  ', ' ')

			unless author_name.include?"&"
				first = author_name.split(/ /).first
				author_name = author_name.gsub("#{first}","")
				author_name += " #{first}"
			end

			unless author_name.blank? or @book.valid?
				author = Author.where("UPPER(unaccent(authors.name)) LIKE '%#{I18n.transliterate(author_name.upcase)}%'").first_or_create do |a|
					a.name = author_name
				end
				@book.author_id = author.id
			end 

			if @book.save
				redirect_to edit_dashboard_book_path(id: @book.id), notice: "Book #{@book.name} created successfully!"
			else
				respond_to do |format|
					format.js
				end
			end
		end
	end

	def destroy
    authorize! :config, :backend
		title = book.name
		if @book.cycles.size == 0 and @book.destroy
			redirect_to request.referrer, notice: "#{title} has been deleted!"
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Unable to delete book #{@book.name}', classes: 'red'}); " }
			end
		end
	end

	private

	def book_params
		params.require(:book).permit(:name, :description, :author_id, :link, :scraping, :image, {features: []})
	end
	def set_book
		@book = Book.find(params[:id])
	end

end
