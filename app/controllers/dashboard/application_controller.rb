class Dashboard::ApplicationController < ActionController::Base
	before_action :authenticate_user!
	protect_from_forgery with: :exception
	before_action :store_user_location!, if: :storable_location?
	rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
	
	layout 'dashboard'


	rescue_from CanCan::AccessDenied do |exception|
	  respond_to do |format|
	    format.json { head :forbidden, content_type: 'text/html' }
	    format.html { redirect_to main_app.root_url, alert: exception.message }
	  end
	end

	private

	def record_not_found
		flash[:alert] = "Você está tentando acessar algo que não existe."
		redirect_back(fallback_location: root_url)
	end

	def storable_location?
		request.get? && is_navigational_format? && !devise_controller? && !request.xhr? 
	end

	def store_user_location!
	  # :user is the scope we are authenticating
	  store_location_for(:user, request.fullpath)
	end

  def after_sign_out_path_for(resource_or_scope)
  	stored_location_for(resource_or_scope) || super
  end




  end
