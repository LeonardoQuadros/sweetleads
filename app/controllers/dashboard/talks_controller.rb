class Dashboard::TalksController < Dashboard::ApplicationController
	before_action :set_talk, only: [:show]

	def show
		authorize! :read, @talk
	end

	private

def set_talk
    @talk = Talk.find_by(user_one_id: [params[:id], current_user.id], user_two_id: [params[:id], current_user.id], cycle: params[:cycle_id])
    # If talk not yet created
    unless @talk
      @cycle = Cycle.find(params[:cycle_id])
      @user = User.find(params[:id])
      # Check if current_user and user are cycle members
      if @cycle.my_users.include? current_user and @cycle.my_users.include? @user
        @talk = Talk.create(user_one: current_user, user_two: @user, cycle: @cycle)
      end
    end
  end
end