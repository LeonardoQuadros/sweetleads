class Dashboard::AuthorsController < Dashboard::ApplicationController

  before_action :set_author, only: [:show, :edit, :update, :destroy]

	def index
    authorize! :config, :backend
		conditions = []
		conditions << "authors.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
		conditions << "UPPER(unaccent(authors.name)) LIKE '%#{I18n.transliterate(params[:name].upcase)}%'" unless params[:name].blank?
		@authors = Author.where(conditions.join(" AND ")).order("created_at desc")
	end

	def show
	end

	def edit
	end

	def update
    authorize! :config, :backend
		if @author.update(author_params)
			if params[:author][:fotos].blank?
				respond_to do |format|
					format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
				end
			else
				redirect_to request.referrer, notice: "Updated"
			end
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: '#{@author.errors.messages}', classes: 'red'}); " }
			end
		end
	end

	def new
		@author = Author.new
	end

	def create
    authorize! :config, :backend
		@author = Author.new(author_params)
		if @author.save
			redirect_to edit_dashboard_author_path(id: @author.id), notice: "Author #{@author.name} created successfully!"
		else
			respond_to do |format|
				format.js
			end
		end
	end

	def destroy
    authorize! :config, :backend
		name = @author.name
		if @author.books.size == 0 and @author.destroy
			redirect_to request.referrer, notice: "#{name} has been deleted!"
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Unable to delete author #{@author.name}', classes: 'red'}); " }
			end
		end
	end

	private

	def author_params
		params.require(:author).permit(:name, :description, :biography)
	end
	def set_author
		@author = Author.find(params[:id])
	end

end
