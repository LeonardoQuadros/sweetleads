class Dashboard::CyclesController < Dashboard::ApplicationController

  before_action :set_cycle, only: [:show, :edit, :update, :destroy, :channels, :members]

	def index
    authorize! :config, :backend
		conditions = []
		conditions << "cycles.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
		@cycles = Cycle.where(conditions.join(" AND ")).order("created_at desc")
	end

	def channels
	end

	def members
		@users = @cycle.users
	end

	def show
		authorize! :read, @cycle
	end

	def edit
	end

	def update
    authorize! :config, :backend
		respond_to do |format|
			if @cycle.update(cycle_params)
      	@cycle.ordenar!
					format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
			else
					format.js {render inline: "M.toast({html: '#{@cycle.errors.messages}', classes: 'red'}); " }
			end
		end
	end

	def new
		@cycle = Cycle.new
	end

	def create
    authorize! :config, :backend
		@cycle = Cycle.new(cycle_params)
		if @cycle.save
      @cycle.ordenar!
			redirect_to edit_dashboard_cycle_path(id: @cycle.id), notice: "Cycle about #{@cycle.book.name} created successfully!"
		else
			respond_to do |format|
				format.js
			end
		end
	end

	def destroy
    authorize! :config, :backend
		title = @cycle.slug
		if @cycle.participations.size < 0 and  @cycle.destroy
			redirect_to request.referrer, notice: "#{title} has been deleted!"
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Unable to delete cycle about #{@cycle.book.name}', classes: 'red'}); " }
			end
		end
	end

	private

	def set_by_slug_cycle
		@cycle = Cycle.find_by(slug: params[:slug])
	end

	def cycle_params
		params.require(:cycle).permit(:description, :book_id, :start_date, :end_date, :slug, :notes, :links).merge(user: current_user)
	end
	def set_cycle
		@cycle = Cycle.find(params[:id])
	end

end
