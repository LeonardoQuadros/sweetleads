class Dashboard::CompletedsController < Dashboard::ApplicationController

  before_action :set_completed, only: [:show, :edit, :update, :destroy]

	def index
		conditions = []
		conditions << "completeds.id IN (#{params[:id].split(/\W+/).uniq.join(', ')})" unless params[:id].blank?
		conditions << "UPPER(unaccent(completeds.name)) LIKE '%#{I18n.transliterate(params[:name].upcase)}%'" unless params[:name].blank?
		@completeds = Completed.where(conditions.join(" AND ")).order("created_at desc")
	end

	def show
	end

	def edit
	end

	def update
		if @completed.update(completed_params)
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Updated!', classes: 'green'}); " }
			end
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: '#{@completed.errors.messages}', classes: 'red'}); " }
			end
		end
	end

	def new
		@completed = Completed.new
	end

	def create
		@completed = Completed.new(completed_params)
		if @completed.save
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Created!', classes: 'green'}); " }
			end
		else
			respond_to do |format|
				format.js
			end
		end
	end

	def destroy
		name = @completed.name
		if @completed.books.size == 0 and @completed.destroy
			redirect_to request.referrer, notice: "#{name} has been deleted!"
		else
			respond_to do |format|
				format.js {render inline: "M.toast({html: 'Unable to delete completed #{@completed.name}', classes: 'red'}); " }
			end
		end
	end

	private

	def completed_params
		params.require(:completed).permit(:content).merge(completed_date: Date.today)
	end
	def set_completed
		@completed = Completed.find(params[:id])
	end

end
