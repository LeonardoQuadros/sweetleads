if channel.present?
	json.extract! channel, :id, :slug, :user_id, :cycle_id, :created_at, :updated_at

	json.messages do
	  json.array! channel.messages.order("created_at desc").page(session[:per_page]) do |message|
	    json.extract! message, :id, :body, :user_id
	    json.date message.created_at.strftime("%d/%m/%y at %I:%M%p")
	    json.user do
	      json.extract! message.user, :id, :name, :email
	    end
	  end
	end
end