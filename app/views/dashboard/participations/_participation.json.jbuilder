json.extract! participation, :id, :user_id, :cycle_id, :created_at, :updated_at

json.user do
  json.extract! participation.user, :id, :name, :email
end