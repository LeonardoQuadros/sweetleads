module CycleHelper

	def cycle_name cycle
		return cycle.blank? ? "" : cycle.slug
	end

	def cycle_description cycle
		cycle.blank? ? "" : "#{cycle.description}".html_safe
	end

	def cycle_period cycle
		cycle.blank? ? "" : "#{l cycle.start_date, format: :short} até #{l cycle.end_date, format: :short}"
	end

	def cycle_joined cycle
		cycle.users.include?current_user
	end

end
