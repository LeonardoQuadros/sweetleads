module TestHelper

	def test_name test
		return test.blank? ? "" : test.name
	end

	def test_can_complete test
		if test.test.blank?
			return true
		else
			Date.today > test.test.date
		end
	end


	def test_completed test
		if test.blank?
			true
		elsif test.users.include?current_user and completed(current_user, test).completed
			true
		else
			false
		end
	end

	def test_actual test
		if (test_completed(test.test) or test.test.blank?) and !test_completed(test)
			true
		else
			false
		end

	end

	def color test
		if test_completed test
			"orange"
		elsif test_completed test.test
			"blue"
		else
			"grey"
		end
	end
end
