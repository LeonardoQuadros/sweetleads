module UserHelper

	def user_name user = current_user
		return user.name.split(/ /).first.titleize
	end

	def user_tests_cycle_completed user, cycle
		amount = cycle.tests.size
		completeds = Completed.where(test_id: cycle.tests.ids, user_id: user.id, completed: true).size
		return [["Completed", completeds], ["Incomplete", amount-completeds]]
	end

end
