module CompletedHelper

	def completed user, test
		Completed.find_by(user_id: user.id, test_id: test.id)
	end

end
