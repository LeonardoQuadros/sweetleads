module AuthorHelper

	def author_name author
		return author.blank? ? "" : author.name
	end

end
