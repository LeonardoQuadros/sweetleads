module BookHelper

	def book_name book
		return book.blank? ? "" : book.name
	end

	def book_link book
		return book.blank? ? "" : book.link
	end

	def book_name_with_author book
		return book.blank? ? "" : "#{self.name} (#{self.author.name} )"
	end


	def book_description book
		return book.blank? ? "" : "#{book.description}".html_safe
	end

end
