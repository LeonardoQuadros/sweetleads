class Participation < ApplicationRecord

	belongs_to :cycle
	belongs_to :user

	validates_presence_of :cycle, :user

	validates_uniqueness_of :user_id, :scope => :cycle_id

end
