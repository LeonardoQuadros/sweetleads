class Cycle < ApplicationRecord
	belongs_to :user
	has_many :tests, dependent: :destroy
	has_many :talks, dependent: :destroy
	has_many :channels, dependent: :destroy
	has_many :participations, dependent: :destroy
	has_many :users, through: :participations

	belongs_to :book
	
	validates_presence_of :book_id, :start_date, :end_date, :user_id, :description
	validates :slug, uniqueness: true, format: { with: /\A[a-zA-Z0-9]+\Z/}

	after_create :general_channel
	after_create :generate_tests

	def general_channel
		self.channels << Channel.create(slug: 'general', user_id: self.user.id)
	end

	def generate_tests
		t1 = Test.create(name: 'Ler <25%> do livro', date: self.start_date+1.week, cycle_id: self.id, dissertative: false)
		t2 = Test.create(name: 'Ler <50%> do livro', date: self.start_date+2.week, cycle_id: self.id, test_id: t1.id, dissertative: false)
		t3 = Test.create(name: 'Ler <75%> do livro', date: self.start_date+3.week, cycle_id: self.id, test_id: t2.id, dissertative: false)
		t4 = Test.create(name: 'Ler <100%> do livro', date: self.start_date+4.week, cycle_id: self.id, test_id: t3.id, dissertative: false)
		t5 = Test.create(name: 'Conhecimento de Ouro (tudo sempre tem algo bom)', date: self.start_date+5.week, cycle_id: self.id, test_id: t4.id, dissertative: true)
		t6 = Test.create(name: 'Autocrítica (Autoconhecimento)', date: self.start_date+6.week, cycle_id: self.id, test_id: t5.id, dissertative: true)
		t7 = Test.create(name: 'Crítica ao Livro (pensamento crítico sobre tudo)', date: self.start_date+7.week, cycle_id: self.id, test_id: t6.id, dissertative: true)

	end

  def my_users
    (self.users+[self.user]).uniq
  end

  def ordenar!
  	self.tests.update_all(order: 99)
  	firsts = self.tests.where(test_id: nil)
  	if firsts.blank?
  		return false
  	else
  		firsts.update_all(order: 0)
  	end
  
  	seconds = self.tests.where(test_id: firsts.ids)
  	if seconds.blank?
  		return false
  	else
  		seconds.update_all(order: 1)
  	end
  
  	thirds = self.tests.where(test_id: seconds.ids)
  	if thirds.blank?
  		return false
  	else
  		thirds.update_all(order: 2)
  	end
  
  	fourths = self.tests.where(test_id: thirds.ids)
  	if fourths.blank?
  		return false
  	else
  		fourths.update_all(order: 3)
  	end
  
  	fifths = self.tests.where(test_id: fourths.ids)
  	if fifths.blank?
  		return false
  	else
  		fifths.update_all(order: 4)
  	end
  
  	sixths = self.tests.where(test_id: fifths.ids)
  	if sixths.blank?
  		return false
  	else
  		sixths.update_all(order: 5)
  	end
  
  	sevenths = self.tests.where(test_id: sixths.ids)
  	if sevenths.blank?
  		return false
  	else
  		sevenths.update_all(order: 6)
  	end

  end

end
