class Test < ApplicationRecord

	belongs_to :cycle
	belongs_to :test, optional: true

	has_many :completeds, dependent: :destroy
	has_many :users, through: :completeds 

	validates_presence_of :name

	scope :ordered, -> { order(order: :asc) }


	def join(user)
		unless self.users.include?user
			begin
				self.users << user
			rescue Exception => e
			end
		end
	end

	def complete(user)
		self.join(user)
		c = self.completeds.where(user_id: user.id).first
		c.update(completed: true, completed_date: Date.today)
		return c 
	end

end