class Book < ApplicationRecord

	validates_presence_of :name, :author_id
	validates_uniqueness_of :name, scope: :author_id

	belongs_to :author
	has_many :cycles

	def name_with_author
		"#{self.name}  (#{self.author.name} )"
	end

end
