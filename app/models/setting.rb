# RailsSettings Model
class Setting < RailsSettings::Base
  source Rails.root.join("config/app.yml")


  def self.verify
    User.all.each do |user|
      Test.all.each do |test|
        test.join(user)
      end
    end

    User.where(admin: false).each do |user|
      c = user.completeds.joins(:test).where(completed: false).where("tests.date < '#{Date.today}'").order("tests.date asc").last
      if c.present?
        SendMailer.email_activity_deadline_coming_end(user, c.test).deliver_later
        puts "enviando ... #{user.name} -- #{c.test&.name}"
        sleep 5
      end
    end
  end


end
