class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      if user.admin
        can :manage, :all
      else
        can :read, Cycle do |t|
          t.user_id == user.id || t.users.where(id: user.id).present?
        end
        can :destroy, Cycle, user_id: user.id

        can [:read, :create], Channel do |c|
          c.cycle.user_id == user.id || c.cycle.users.where(id: user.id).present?
        end
        can [:destroy, :update], Channel do |c|
          c.cycle.user_id == user.id || c.user_id == user.id
        end

        can [:read], Talk do |t|
          t.user_one_id == user.id || t.user_two_id == user.id
        end

        can [:create, :destroy], Participation do |t|
          t.cycle.user_id == user.id
        end
      end
    end
  end
end