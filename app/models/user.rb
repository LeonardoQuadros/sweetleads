class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  has_many :cycles
  has_many :messages
  has_many :talks, dependent: :destroy

  has_many :participations, dependent: :destroy
  has_many :member_cycles, through: :participations, :source => :cycle

  has_many :completeds, dependent: :destroy
  has_many :tests, through: :completeds 



  def my_cycles
    self.cycles + self.member_cycles
  end

end
