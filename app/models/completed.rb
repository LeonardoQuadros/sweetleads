class Completed < ApplicationRecord

	belongs_to :user
	belongs_to :test

	validates_uniqueness_of :user_id, scope: :test_id

	validate :to_complete


	def to_complete
		if self.completed and self.content.blank? and self.test.dissertative
			errors.add(:erro, '~> Content can not be empty.')
			return false
		elsif test.test.present? and Date.today < test.test.date
			errors.add(:to_fast, '~> Take it easy, kid. Now only next week.')
			return false
		else
			return true
		end
	end

end
