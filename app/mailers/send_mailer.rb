class SendMailer < ApplicationMailer
    
    def email_confirmation_commitment(user, cycle)
        @user = user
        @cycle = cycle
        mail( to: "#{@user.email}", subject: "#{Setting.email_confirmation_commitment_subject}".gsub("@user", @user.name).gsub("@cycle", @cycle.slug)) do |format|
          format.html
        end
    end
    
    def email_activity_completion(user, test)
        @user = user
        @test = test
        @cycle = test.cycle
        mail( to: "#{@user.email}", subject: "#{Setting.email_activity_completion_subject}".gsub("@user", @user.name).gsub("@cycle", @cycle.slug).gsub("@test", @test.name)) do |format|
          format.html
        end
    end
    
    def email_activity_deadline_coming_end(user, test)
        @user = user
        @test = test
        @cycle = test.cycle
        mail( to: "#{@user.email}", subject: "#{Setting.email_activity_deadline_coming_end_subject}".gsub("@user", @user.name).gsub("@cycle", @cycle.slug).gsub("@test", @test.name)) do |format|
          format.html
        end
    end
end
