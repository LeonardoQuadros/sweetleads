class ApplicationMailer < ActionMailer::Base
  default from: 'cursos@sweetleads.com.br'
  layout 'mailer'
end
