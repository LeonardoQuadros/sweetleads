require 'rest-client'
require 'nokogiri'
require 'open-uri'

class BookService

	def self.get_book (title)

		begin
			unless title.blank?
				html = open("https://busca.saraiva.com.br/busca?q="+"#{I18n.transliterate(title)}")
				content = Nokogiri::HTML(html, nil, Encoding::UTF_8.to_s)
				list = {}
				list[:image] = "#{content.css('.neemu-products-container').first.css('.nm-product-img-container').first.css('.nm-product-img').first['data-original']}"
				content.css('.neemu-products-container').first.css("li").first.css(".nm-product-info").each do |cell|
					list[:title] = "#{cell.css('.nm-product-name').first.css('a').first.content}".to_s.gsub(/[\r\n\t]+/, ' ')
					list[:author] = "#{cell.css('.nm-product-subtitle').first.content}".to_s.gsub(/[\r\n\t]+/, ' ')
					list[:link] = "https:#{cell.css('.nm-product-name').first.xpath('a').first['href']}"

					unless list[:link].blank?
						html_book = open(list[:link]) 
						content_book = Nokogiri::HTML(html_book, nil, Encoding::UTF_8.to_s)
						list[:title] = "#{content_book.css('#pdp-info').first.css('h1').first.content}".to_s.gsub(/[\r\n\t]+/, ' ')
						list[:description] = content_book.css("#product_description").first.css(".tresspassing").first.content.to_s.gsub(/[\r\n\t]+/, ' ').gsub("  ", "")
						list[:image] = "#{content_book.css("#pdp-slider").first.css('.img-slider').first.css('li').first.css('img').first['src']}"

						features = {}
						content_book.css("#product_attributes").first.css(".tresspassing").first.css("li").each do |li|
							key = li.css("dl").first.css("dt").first.content
							value = li.css("dl").first.css("dd").first.content
							features["#{key}"] = value
						end

						list[:features] = features

					end 
				end
			end
			
		rescue Exception => list
			Rails.logger.debug("  ============== Exception:  #{list} ")
		end

		return list
	end

end