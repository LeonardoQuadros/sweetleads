class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message)
    m = message.messagable
    chat_name = (m.class == Channel)? "#{m.cycle.id}_channels_#{m.id}" : "#{m.cycle.id}_talks_#{m.id}"
    ActionCable.server.broadcast(chat_name, {
                                          message: message.body,
                                          date: message.created_at.strftime("%d/%m/%y at %I:%M%p"),
                                          name: message.user.name
                                        })
  end
end