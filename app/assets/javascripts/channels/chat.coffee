window.change_chat = (id, type, cycle_id) ->
  if App.chat != undefined
    App.chat.unsubscribe()

  setTimeout (->
    App.chat = App.cable.subscriptions.create { channel: "ChatChannel", id: id , type: type, cycle_id: cycle_id},
      received: (data) ->
        window.add_message(data.message, data.date, data.name)
  ), 100	    