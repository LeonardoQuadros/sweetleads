# Load messages getting the first channel
$(document).on 'turbolinks:load', ->
  if $('ul.channels .collection-item:first').attr('channel') != undefined
    window.open($('ul.channels .collection-item:first').attr('channel'), 'channels')

# Get channel messages when clicked
  $('body').on 'click', 'a.open_channel', (e) ->
    window.open(e.target.id, 'channels')


# Get talk messages when clicked
$(document).on 'turbolinks:load', ->
  $('body').on 'click', 'a.open_user', (e) ->
    window.open(e.target.id, 'talks')