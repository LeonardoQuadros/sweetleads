set_chat = (data, id, type) ->
    $(".chat_name").html('<h5 class="green-text animated bounceInLeft faster no-margin f20">#'+data.replace(' ','_').toLowerCase()+'</h5>'+'<span id="messagable" messagable_id="'+id+'" messagable_type="'+type+'"></span>')

clean_messages = () ->
    $(".messages").html("")
    $(".chat_name").html("")
    setTimeout (->
      count = $('.message').length
      $('.message').each (index) ->
        animated_index = (count-index)
        if(animated_index < 10)
          $(this).addClass ' delay-'+((animated_index)*100).toString();
        return
      return
    ), 100

window.add_message = (message, message_date, name) ->
  $(".messages").append('<div class="message col s12 radius green lighten-5" style="margin-top: 5px;">' +
                          '<div class="col m2 l1">' +
                            '<i class="material-icons prefix right profile_icon  ">account_circle</i>'+
                          '</div>'+
                          '<div class="col m10 s9">'+
                            '<div class="row no-margin ">'+
                              '<b class="span f15">#' + name.replace(' ','_').toLowerCase() + '</b> <span class="date right">' + message_date + '</span>'+
                            '</div>' +
                            '<div class="row no-margin"><span class="span content_message">' + message + '</span></div>'+
                          '</div>'+
                        '</div>')
  $(".messages").animate({ scrollTop: $('.messages').prop("scrollHeight")}, 1);

window.prepend_message = (message, message_date, name) ->
  $(".messages").prepend('<div class="message animated bounceInDown col s12 radius green lighten-5" style="margin-top: 5px;">' +
                          '<div class="col m2 l1">' +
                            '<i class="material-icons prefix right profile_icon  ">account_circle</i>'+
                          '</div>'+
                          '<div class="col m10 s9">'+
                            '<div class="row no-margin ">'+
                              '<b class="span f15">#' + name.replace(' ','_').toLowerCase() + '</b> <span class="date right">' + message_date + '</span>'+
                            '</div>' +
                            '<div class="row no-margin"><span class="span content_message">' + message + '</span></div>'+
                          '</div>'+
                        '</div>')

window.open = (id, type) ->
  clean_messages()
  $.ajax "/dashboard/" + type + "/" + id,
      type: 'GET'
      contentType:'application/json',
      dataType: 'json'
      data: {cycle_id: $(".cycle_id").val()}
      success: (data, text, jqXHR) ->
        if type == "talks"
          set_chat(data['user']['name'], id, type)
        else
          set_chat(data['slug'], id, type)

        window.change_chat(id, type, $(".cycle_id").val())

        if(data['messages'])
          for message, index in data['messages']
            do ->
              window.prepend_message(message['body'], message['date'], message['user']['name'])
              $(".messages").animate({ scrollTop: $('.messages').prop("scrollHeight")}, 1);
      error: (jqXHR, textStatus, errorThrown) ->
        M.toast({html: 'Problem to get '+type+ ' informations &nbsp;<b>:(</b>', classes: 'green'});

    return false




window.add = (slug, id, type) ->
  additional = if type == "channel" then "#" else ""
  $('.' + type + 's').append('<li class="collection-item ' + type + '_' + id + '">' +
                              '  <div>' +
                                    '<a href="#" class="open_' + type + '">' +
                                        '<span id="' + id + '" class="span">' + additional + slug + '</span>' +
                                    '</a>' +
                                    '<a class="right remove_' + type + '" href="#" id="' + id + '">' +
                                        '<i class="material-icons" id="' + id + '">settings</i>' +
                                    '</a>' +
                                '</div>' +
                            '</li>')

